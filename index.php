<?php
function resize_image($file_path, $max_height) {
    list($width, $height) = getimagesize($file_path);
    $aspect_ratio = $width / $height;
    $new_width = $aspect_ratio * $max_height;
    $new_height = $max_height;
    $src = imagecreatefromstring(file_get_contents($file_path));
    $dst = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    imagedestroy($src);
    return $dst;
}

function add_file_to_zip($file_path, $zip) {
    $handle = fopen($file_path, 'r');
    $content = fread($handle, filesize($file_path));
    fclose($handle);
    $zip->addFromString(basename($file_path), $content);
}

$folder_path = './'; // zip dosyasına dahil edilecek klasörün yolu
$zip_file_name = 'zipped_files.zip'; // zip dosyasının adı
$max_height = 800; // maksimum yükseklik

// tüm görüntüleri boyutlandır ve zip'e ekle
$zip = new ZipArchive();
if ($zip->open($zip_file_name, ZipArchive::CREATE) === TRUE) {
    $files = scandir($folder_path);
    foreach ($files as $file) {
        if (is_file($folder_path . $file) && in_array(strtolower(pathinfo($file, PATHINFO_EXTENSION)), ['jpg', 'jpeg', 'png', 'gif'])) {
            $resized = resize_image($folder_path . $file, $max_height);
            $new_file = 'resized_' . $file;
            imagejpeg($resized, $folder_path . $new_file, 80);
            imagedestroy($resized);
            add_file_to_zip($folder_path . $new_file, $zip);
            unlink($folder_path . $new_file);
        }
    }
    $zip->close();
    echo 'Zip dosyası başarıyla oluşturuldu!';
} else {
    echo 'Zip dosyası oluşturulamadı!';
}
?>